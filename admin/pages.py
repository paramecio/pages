from paramecio2.libraries.generate_admin_class import GenerateAdminClass
#from paramecio2.libraries.db.adminutils import make_admin_url
#from paramecio.citoplasma.urls import make_url
from paramecio2.libraries.i18n import I18n
from settings import config
from modules.pages.models import pages

from paramecio2.libraries.db.coreforms import BaseForm
from paramecio2.modules.admin import admin_app, t as admin_t
from flask import url_for, g

@admin_app.route('/admin/pages/', methods=['GET', 'POST'])
def admin_pages():
    
    t=admin_t

    conn=g.connection
    
    page=pages.Page(conn)
    
    page.enctype=True

    page.fields['slugify'].name_form=BaseForm

    page.fields['text'].extra_parameters[0].t=t
    
    #url=make_admin_url('pages')
    url=url_for('admin_app.admin_pages')
    
    admin=GenerateAdminClass(page, url, t)
    
    admin.list.fields_showed=['id', 'title', 'slugify']
    
    form_admin=admin.show()
    
    #return admin.show()
    if type(form_admin).__name__=='str':
        
        return t.load_template('content.phtml', title=I18n.lang('pages', 'pages', 'Pages'), contents=form_admin, path_module='/admin/pages')    
    else:
        
        return form_admin
